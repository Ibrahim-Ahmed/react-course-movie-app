import React, { Component } from "react";

import { Provider as PaperProvider } from "react-native-paper";

import { Provider } from "react-redux";

import { Navigator } from "movies/src/navigation";

import initialState from "reduxfiles/reducers/initialState";

import configureStore from "reduxfiles/configureStore";

import Api from "utils/Api";

const store = configureStore(initialState);

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <PaperProvider>
                    <Navigator />
                </PaperProvider>
            </Provider>
        );
    }
}

export default App;
