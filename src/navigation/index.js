import React from "react";

import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import MovieList from "movies/src/screens/home/movie-list";
import Profile from "movies/src/screens/home/profile";

const HomeStackNavigator = createStackNavigator({
    Movies: {
        screen: MovieList
    },
    ProfileScreen: {
        screen: Profile
    }
});

export const Navigator = createAppContainer(HomeStackNavigator);
