import React, { Component } from "react";

import { View, Text, Dimensions } from "react-native";

import { Title, Appbar, Searchbar, Card, Button } from "react-native-paper";

import { connect } from "react-redux";

import { bindActionCreators } from "redux";

import * as moviesActions from "reduxfiles/actions/movies";

import Styles from "styles/screens/home/movie-list";

import Icon from "react-native-vector-icons/MaterialIcons";

import Carousel from "react-native-snap-carousel";

const { width } = Dimensions.get("window");

const sliderWidth = width - 60;

class MovieList extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            header: () => {
                return (
                    <Appbar.Header>
                        <Appbar.Action icon="menu" />
                        <Appbar.Content title="Movies" subtitle="2019" />
                        <Appbar.Action
                            icon="magnify"
                            onPress={this._handleMore}
                        />
                        <Appbar.Action
                            icon="dots-vertical"
                            onPress={() => navigation.push("ProfileScreen")}
                        />
                    </Appbar.Header>
                );
            }
        };
    };

    state = { search: "" };

    renderCards(item) {
        return (
            <Card style={Styles.card}>
                <Card.Title title={item.title} subtitle={item.year} />

                <Card.Cover source={{ uri: item.poster }} />
            </Card>
        );
    }

    componentDidMount() {
        this.props.actions.fetchMovies();
    }

    render() {
        return (
            <View style={Styles.container}>
                <View style={Styles.topContent}>
                    <Text style={Styles.message}>
                        Hello, what do you want to watch?
                    </Text>
                    <Searchbar
                        placeholder="Search"
                        onChangeText={query => {
                            this.setState({ search: query });
                        }}
                        value={this.state.search}
                    />
                </View>
                <View style={Styles.bottomContent}>
                    <Text style={Styles.recommendedText}>
                        RECOMMENDED FOR YOU
                    </Text>

                    <Carousel
                        ref={c => {
                            this._carousel = c;
                        }}
                        data={this.props.list}
                        renderItem={({ item }) => this.renderCards(item)}
                        sliderWidth={sliderWidth}
                        itemWidth={sliderWidth / 1.5}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        list: state.movies.list
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(moviesActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MovieList);
