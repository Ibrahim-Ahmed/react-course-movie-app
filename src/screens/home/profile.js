import React, { Component } from "react";

import { View, Text, Animated, ScrollView } from "react-native";

import Styles from "styles/screens/home/profile";

import { Appbar } from "react-native-paper";

//import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";

class Profile extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null
        };
    };

    state = {
        scrollY: new Animated.Value(0)
    };

    render() {
        const height = this.state.scrollY.interpolate({
            inputRange: [0, 28, 56],
            outputRange: [56, 28, 0],
            extrapolate: "clamp"
        });

        const opacity = this.state.scrollY.interpolate({
            inputRange: [0, 28 / 2, 28],
            outputRange: [1, 1, 0],
            extrapolate: "clamp"
        });

        const translateY = this.state.scrollY.interpolate({
            inputRange: [0, 28, 56],
            outputRange: [0, -28, -56],
            extrapolate: "clamp"
        });

        return (
            <View style={Styles.container}>
                <Animated.View
                    style={{
                        transform: [
                            {
                                translateY: translateY
                            }
                        ]
                    }}>
                    <Appbar.Header style={[Styles.header, { height }]}>
                        <Appbar.Action icon="menu" />
                        <Appbar.Content title="Profile" subtitle="Adam" />
                        <Appbar.Action
                            icon="magnify"
                            onPress={this._handleMore}
                        />
                        <Appbar.Action
                            icon="dots-vertical"
                            onPress={this._handleMore}
                        />
                    </Appbar.Header>
                </Animated.View>
                <ScrollView
                    onScroll={Animated.event([
                        {
                            nativeEvent: {
                                contentOffset: {
                                    y: this.state.scrollY
                                }
                            }
                        }
                    ])}
                    contentContainerStyle={Styles.scrollview}>
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                    <View
                        style={{
                            height: 48,
                            backgroundColor: "green",
                            marginTop: 16
                        }}
                    />
                </ScrollView>
            </View>
        );
    }
}

export default Profile;
