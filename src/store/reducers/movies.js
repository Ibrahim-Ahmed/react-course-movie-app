import * as types from "../actions/types";

import initialState from "./initialState";

export default function(state = initialState.movies, action) {
    switch (action.type) {
        case types.GET_MOVIES:
            return {
                ...state,
                list: action.list
            };
        default:
            return {
                ...state
            };
    }
}
