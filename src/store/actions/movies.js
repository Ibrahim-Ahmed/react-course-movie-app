import * as types from "./types";

import Api from "utils/Api";

export function fetchMovies() {
    return async (dispatch, getState) => {
        try {
            let response = await Api.get(`MinaSamir11/Test/list`);

            return dispatch(setMoviesList(response.data));
        } catch (ex) {
            dispatch(setMoviesList([]));
        }
    };
}

function setMoviesList(data) {
    return {
        list: data,
        type: types.GET_MOVIES
    };
}
