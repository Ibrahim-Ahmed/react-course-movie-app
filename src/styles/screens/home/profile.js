import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    header: {
        flex: 0,
        position: "absolute",
        top: 0,
        left: 0,
        width
    },
    scrollview: {
        paddingTop: 56
    }
});

export default styles;
