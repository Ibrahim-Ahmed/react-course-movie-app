import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#5CA1D4"
    },
    message: {
        fontSize: 30,
        fontWeight: "700",
        color: "#FFF"
    },
    topContent: {
        paddingLeft: 45,
        paddingRight: 45,
        paddingTop: 40,
        flex: 0.25
    },
    bottomContent: {
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        backgroundColor: "#283546",
        flex: 0.75,
        padding: 30,
        paddingTop: 30
    },
    recommendedText: {
        color: "#FFF"
    },
    card: {
        marginTop: 25,
        marginRight: 16,
        width: 200,
        height: 300
    }
});

export default styles;
